Example use :
```
python run.py -g ../climelioth/data/in/geometry/buildings.json -p data/in/solar_areas_modules.xlsx -o data/out/PV_results.csv

```

Optional weather parameter:
```
-w
```
Accepts EPW file, CSV file or RT2012 zone 
Default RT2012 for Paris region