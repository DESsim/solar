import pandas as pd
import numpy as np
import json
from pvlib import pvsystem
import sys
import os
from epw import epw
import math

script_path = os.path.abspath(__file__)
dir_path = os.path.dirname(os.path.dirname(os.path.dirname(script_path)))
sys.path.append(dir_path+"/climelioth/python/")

import climelioth


if len(sys.argv) > 1:
    jsonpath = sys.argv[1]
    xlsxpath = sys.argv[2]
    outputcsv = sys.argv[3]

    
def calcPV(solar_results, solar_parameters, context):
    print("Calculating PV yield...")
    #Process solar results
    df = solar_results
    df.columns = ['time', 'building_id', "volume_id", 'level', 'area', 'type', 'azimuth', 'tilt', 'rad']

    df['rad_area'] = df['rad']*df['area']

    df['azimuth_bin'] = 'NE'
    df.loc[ (df['azimuth'] > -90) & (df['azimuth'] < 0) , 'azimuth_bin'] = 'SE'
    df.loc[ (df['azimuth'] > 0) & (df['azimuth'] < 90) , 'azimuth_bin'] = 'SO'
    df.loc[ (df['azimuth'] > 90) , 'azimuth_bin'] = 'NO'

    df_rad = df.groupby(['building_id', "volume_id", 'time', 'type', 'azimuth_bin', 'tilt'], as_index=False)['rad_area'].sum()

    df_areas = df.groupby(['building_id', "volume_id", 'time', 'type', 'azimuth_bin', 'tilt'], as_index=False)['area'].sum()
    df_areas = df_areas.groupby(['building_id', "volume_id", 'type', 'azimuth_bin', 'tilt'], as_index=False)['area'].first()

    df_rad = pd.merge(df_rad, df_areas, on = ['building_id', "volume_id", 'type', 'azimuth_bin', 'tilt'])
    df_rad['rad'] = df_rad['rad_area']/df_rad['area']
 
    #Process solar parameters
    solar_areas = solar_parameters
    solar_areas['azimuth'] = solar_areas['orientation']

    solar_areas['azimuth_bin'] = 'NE'
    solar_areas.loc[ (solar_areas['azimuth'] > -90) & (solar_areas['azimuth'] < 0) , 'azimuth_bin'] = 'SE'
    solar_areas.loc[ (solar_areas['azimuth'] > 0) & (solar_areas['azimuth'] < 90) , 'azimuth_bin'] = 'SO'
    solar_areas.loc[ (solar_areas['azimuth'] > 90) , 'azimuth_bin'] = 'NO'
    
    solar = pd.merge(solar_areas[['building_id', "volume_id", "id_surface", 'surface_type', 'surface de module', 'rendement', 'azimuth_bin', "Module"]],
         df_rad[['building_id', 'time', 'type', 'azimuth_bin', 'rad']],
         left_on = ['building_id', 'surface_type', 'azimuth_bin'],
         right_on=['building_id', 'type', 'azimuth_bin'])


    # PV models
    # Simpl
    solar['p_pv_simpl'] = solar['rad']*solar['surface de module']*solar['rendement']*0.85/1000
    # PVLIB
    solar['p_pv'] = np.nan
    grouped = solar.groupby(["building_id","surface_type","azimuth_bin"])
    for name, hours in grouped:
        if isinstance(hours.Module.iloc[0], str): #Check if Module name is defined
            nsurfs = int(hours.shape[0]/8760)
            if name[1] == "wall":
                celltempmodel = "insulated_back_polymerback"
            else:
                celltempmodel = "roof_mount_cell_glassback"
                
            # print(hours.Module.iloc[0],
            #             hours.rad.astype(float).shape,
            #             np.tile(context.wind_speed,nsurfs).shape,
            #             np.tile(context.air_temperature-273.5,nsurfs).shape)
            solar.loc[(solar.building_id == name[0]) & (solar.surface_type == name[1]) & (solar.azimuth_bin == name[2]),"p_pv"] = (getDC(hours.Module.iloc[0],
                                                                                                                                hours.rad.astype(float),
                                                                                                                                np.tile(context.wind_speed,nsurfs),
                                                                                                                                np.tile(context.air_temperature-273.5,nsurfs),
                                                                                                                                celltempmodel, Norm=True)*hours["surface de module"]*0.85/1000).values
        else:
            print("PV module not defined for {}".format(name))   

    return solar
    
    
def setContext(climeliothwea):
    delta_t = 3600
    context = climelioth.context.Context(delta_t=delta_t)
    context.set_weather(air_temperature = climeliothwea["air_temperature"],
                    water_temperature = climeliothwea["water_temperature"],
                    sky_temperature = climeliothwea["sky_temperature"],
                    air_humidity = climeliothwea["air_humidity"],
                    direct_normal_irradiation = climeliothwea["direct_normal_irradiation"],
                    diffuse_horizontal_irradiation = climeliothwea["diffuse_horizontal_irradiation"],
                    solar_elevation = climeliothwea["solar_elevation"],
                    solar_azimuth = climeliothwea["solar_azimuth"],
                    wind_speed = climeliothwea["wind_speed"])
    return context
    
def loadBuildings(jsonpath="/home/ubuntu/dev/climelioth/data/in/geometry/buildings.json"):
    try:
        with open(jsonpath) as file:
            buildings = json.load(file)
            file.close()
    except:
        buildings = pd.DataFrame()
        print("Cannot read building geometry... proceeding with empty geometry")
    return buildings
 
def calcRad(buildings,solar_parameters, context, Save=False,outfile="data/out/solar_results.csv"):
    timebase = pd.date_range(start=pd.to_datetime('2005-01-01 00:00:00', format='%Y-%m-%d %H:%M:%S'),
                            end=pd.to_datetime('2005-12-31 23:00:00', format='%Y-%m-%d %H:%M:%S'),
                            freq='H')
    solar_areas = solar_parameters
    solar_results = []
    # Load the masks
    print("Calculating solar radiation...")
    for b in buildings:
        print("Building", b['building_id'])
        volumes = b['volumes']
        for v in volumes:
            zones = v['zones']
            for z in zones:
                floors = z['floor']
                for f in floors:
                    walls = f['walls']
                    for w in walls:
                        if w['adjacent'] is False:
                            if w['azimuth'] < 90 and w['azimuth'] > -90:
                            
                                face = climelioth.face.Face(area=1, azimuth=w['azimuth'], tilt=90)
                                mask = climelioth.solar_mask.AzimuthElevationMask(w['mask'])
                                face.add_masks([mask])
                                isr = face.compute_incident_solar_radiation(context)
                                df = pd.DataFrame({'time': timebase,
                                                   'building_id': b['building_id'],
                                                   'volume_id': 0, #not using volume id for walls
                                                   'level': z['level'],
                                                   'area': w['area'],
                                                   'type': 'wall',
                                                   'azimuth': w['azimuth'],
                                                   'tilt': 90,
                                                   'rad': np.round(isr)})

                                solar_results.append(df)
                                
                    for r in f['roofs']:
                        pars = solar_areas.loc[ (solar_areas['building_id'] == b['building_id']) & (solar_areas['surface_type'] == 'roof') & (solar_areas['volume_id'] == v['volume_id'])]
                        
                        if pars.shape[0] > 0:
                            
                            for i in range(pars.shape[0]):
                                
                                p = pars.iloc[i].to_dict()
                                face = climelioth.face.Face(area=1, azimuth=p['orientation'], tilt=p['inclinaison'])
                                mask = climelioth.solar_mask.AzimuthElevationMask(r['mask'])
                                face.add_masks([mask])
                                isr = face.compute_incident_solar_radiation(context)
                                # print( b['building_id'],r['area'],isr.sum()/1000)
                                df = pd.DataFrame({'time': timebase,
                                                       'building_id': b['building_id'],
                                                       'volume_id': v['volume_id'],
                                                       'level': z['level'],
                                                       'area': r['area'],
                                                       'type': 'roof',
                                                       'azimuth': p['orientation'],
                                                       'tilt': p['inclinaison'],
                                                       'rad': np.round(isr)})

                                solar_results.append(df)

    # ---------------------------
    # Find installations that are not present in the geometry (=external buildings)
    # Compute the incident solar radiation free of all masks
    buildings_in_geometry = [b['building_id'] for b in buildings]

    for b in solar_parameters['building_id']:
        if b not in buildings_in_geometry:

            print("Building", b)

            p = solar_parameters[ solar_parameters['building_id'] == b ].to_dict('records')[0]

            face = climelioth.face.Face(area=1, azimuth=p['orientation'], tilt=p['inclinaison'])
            isr = face.compute_incident_solar_radiation(context)

            df = pd.DataFrame({'time': timebase,
                                                   'building_id': b,
                                                   'volume_id': 0,
                                                   'level': 1,
                                                   'area': p['surface de module'],
                                                   'type': p['surface_type'],
                                                   'azimuth': p['orientation'],
                                                   'tilt': p['inclinaison'],
                                                   'rad': np.round(isr)})

            
            solar_results.append(df)


    # ---------------------------
    solar_results = pd.concat(solar_results)
    return solar_results   

def getDC(CECmodule,POAi, Wspd, Atemp, celltempmodel = 'insulated_back_polymerback', Norm=True,script_path=script_path):
    from pvlib import pvsystem
    #Default values
    AMa = 1.5 #Air mass
    M = np.polyval([-1.26E-4, 2.816E-3, -0.024459, 0.086257, 0.918093],AMa)
    EgRef = 1.121 #suggested for Si-modules
    dEgdT=-0.0002677 #Suggested for Si-modules
    
    #Retrieve module
    modules = pvsystem.retrieve_sam(path=os.path.join(os.path.dirname(os.path.dirname(script_path)),"data","in","CEC_Modules.csv"))
    modules.loc["Eff",:] = (modules.loc["I_mp_ref",:]*modules.loc["V_mp_ref",:]/1000)/modules.loc["A_c",:]
    module = modules[CECmodule]
    
    #Calculate Cell Temperature
    temps = pvsystem.sapm_celltemp(POAi, Wspd, Atemp, celltempmodel)
    #De Soto
    photocurrent, saturation_current, resistance_series, resistance_shunt, nNsVth = (
    pvsystem.calcparams_desoto(POAi,
                                         temp_cell=temps['temp_cell'],
                                         alpha_sc=module['alpha_sc'],
                                         a_ref=module['a_ref'],
                                         I_L_ref = module['I_L_ref'],
                                         I_o_ref = module['I_o_ref'],
                                         R_sh_ref = module['R_sh_ref'],
                                         R_s = module['R_s'],
                                         EgRef =EgRef,
                                         dEgdT = dEgdT))
    
    #Single Diode
    single_diode_out = pvsystem.singlediode(photocurrent, saturation_current, resistance_series, resistance_shunt, nNsVth)
    p_mp = single_diode_out['p_mp']
    
    #Normalize by area
    if Norm:
        p_mp = p_mp / module["A_c"]
    return p_mp
    
    
###

def RadMap(buildings, context, Save=False,outfile="data/out/solar_results.csv"):
    timebase = pd.date_range(start=pd.to_datetime('2005-01-01 00:00:00', format='%Y-%m-%d %H:%M:%S'),
                            end=pd.to_datetime('2005-12-31 23:00:00', format='%Y-%m-%d %H:%M:%S'),
                            freq='H')
    solar_results = []
    # Load the masks
    print("Calculating solar radiation...")
    for b in buildings:
        print("Building", b['building_id'])
        volumes = b['volumes']
        for v in volumes:
            zones = v['zones']
            for z in zones:
                floors = z['floor']
                for f in floors:
                    walls = f['walls']
                    for w in walls:
                        if w['adjacent'] is False:                           
                            face = climelioth.face.Face(area=1, azimuth=w['azimuth'], tilt=90)
                            mask = climelioth.solar_mask.AzimuthElevationMask(w['mask'])
                            face.add_masks([mask])
                            isr = face.compute_incident_solar_radiation(context)    
                            solar_results.append(isr.sum()/1000)
                    for r in f['roofs']:
                        face = climelioth.face.Face(area=1, azimuth=w['azimuth'], tilt=0)
                        mask = climelioth.solar_mask.AzimuthElevationMask(r['mask'])
                        face.add_masks([mask])
                        isr = face.compute_incident_solar_radiation(context)
                        solar_results.append(isr.sum()/1000)
    return np.array(solar_results)
                                
if __name__ == "__main__":
    xlsxpath = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\production\solaire_pv\in\test\solar_areas_modules.xlsx"
    # #xlsxpath = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\production\solaire_pv\in\1\solar_areas_modules.xlsx"
    # jsonpath = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\geometrie\out\a3735ef6-ffaa-11e9-a33d-d4e099056fc8\buildings.json"
    # weather = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\meteo\out\Paris_Montsouris-hour.epw\Paris_Montsouris-hour.epw.csv"
    # #weather = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\30_Prospection\32 - Concours et offres\BAPK24001 - HUGPN - APHP\04 - Concours\02 - Production\Calculs\energie\meteo\out\HUGPN-radwh.epw\HUGPN-radwh.epw.csv"
    # climeliothwea = pd.read_csv(weather,comment='#')
    # context = setContext(climeliothwea)
    # try:
    #     buildings = loadBuildings(jsonpath)
    # except:
    #     print("No building geometry... proceeding with empty geometry")
    #     buildings = pd.DataFrame()
    solar_parameters = pd.read_excel(xlsxpath)

    solar_results = calcRad(buildings, solar_parameters, context)
    PV_results = calcPV(solar_results, solar_parameters, context)
    
    
    summary = PV_results.groupby(["building_id","surface_type","azimuth_bin","surface de module","id_surface"]).sum()[["rad","p_pv","p_pv_simpl"]].reset_index()
    pmax = PV_results.groupby(["building_id","surface_type","azimuth_bin","surface de module","id_surface"]).max()[["p_pv"]].reset_index().rename({"p_pv":"PV max power (kW)"},axis=1)
    summary = pd.merge(solar_parameters,summary,on=["building_id","surface_type","azimuth_bin","surface de module","id_surface"])
    summary = pd.merge(summary,pmax,on=["building_id","surface_type","azimuth_bin","surface de module","id_surface"])
    summary["rad"] /= 1000 #W/m2 to kW/m2
    summary = summary.drop(["p_pv_simpl"],axis=1)
    summary = summary.rename({"rad":"Irradiation (kWh/m2)","p_pv": "PV generation (kWh)"},axis=1)

    #PV_results.to_csv(outputcsv)