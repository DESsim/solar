import sys
import os
import getopt
import pandas as pd


def main(argv):

    # Find the path of the script
    script_path = os.path.abspath(__file__)
    dir_path = os.path.dirname(script_path)
    sys.path.append(dir_path + "/python/")
    import solar
    
    #Find the path of the dev repository
    sys.path.append(os.path.dirname(dir_path) + "/weather-import/python/")
    import weatherimport
        
    weather = ""
    geometry = ""
    
    try:
        opts, args = getopt.getopt(argv, "g:w:p:o:", ["geometry=", "weather=", "solar_parameters_file=", "output_csv="])
    except getopt.GetoptError:
        print('run.py -g <optional_geometry_file_path> -w <optional_weather_file_path> -s <std_hyps_file_path> -o <output_file_path>')
        sys.exit(2)
     
    for opt, arg in opts:
        if opt in ("-g", "--geometry_file_path"):
            geometry = arg
        elif opt in ("-p", "--solar_parameters_path"):
            solar_parameters_file = arg
        elif opt in ("-o", "--output_file_path"):
            output_csv = arg
        elif opt in ("-w", "--weather_file_path"):
            weather = arg
    print(geometry,solar_parameters_file,weather)
    buildings = solar.loadBuildings(geometry)
    solar_parameters = pd.read_excel(solar_parameters_file)
    
    if weather[-4:] == ".csv":
        climeliothwea = pd.read_csv(weather,comment='#')
    elif weather[-4:] == ".epw":
        climeliothwea = weatherimport.getEPW(weather)
    elif weather == "":
        climeliothwea = weatherimport.getRT2012()
    else:
        climeliothwea = weatherimport.getRT2012(weather)
    
    context = solar.setContext(climeliothwea)
    
    #Get Solar Radiation using Climelioth
    solar_results = solar.calcRad(buildings, solar_parameters, context)

    #Apply PV models
    PV_results = solar.calcPV(solar_results, solar_parameters, context)
    
    #Write result to file
    PV_results.to_csv(output_csv)
    
    #Create summary
    summary = PV_results.groupby(["building_id","surface_type","azimuth_bin","surface de module","id_surface"]).sum()[["rad","p_pv","p_pv_simpl"]].reset_index()
    pmax = PV_results.groupby(["building_id","surface_type","azimuth_bin","surface de module","id_surface"]).max()[["p_pv"]].reset_index().rename({"p_pv":"PV max power (kW)"},axis=1)
    summary = pd.merge(solar_parameters,summary,on=["building_id","surface_type","azimuth_bin","surface de module","id_surface"])
    summary = pd.merge(summary,pmax,on=["building_id","surface_type","azimuth_bin","surface de module","id_surface"])
    summary["rad"] /= 1000 #W/m2 to kW/m2
    summary = summary.drop(["p_pv_simpl"],axis=1)
    summary = summary.rename({"rad":"Irradiation (kWh/m2)","p_pv": "PV generation (kWh)"},axis=1)
    summary.to_excel(output_csv.replace(".csv","_summary.xlsx"))
    
if __name__ == "__main__":
    main(sys.argv[1:])